package com.epam.tat.lesson7;

import com.epam.tat.lesson7.cli.TestRunnerOptions;
import com.epam.tat.lesson7.utils.FileTools;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class CustomTestNgRunner {

    private CustomTestNgRunner(String[] args) {
        parseCli(args);
    }

    public static void main(String[] args) {
        new CustomTestNgRunner(args).runTests();
    }

    private void parseCli(String[] args) {
        TestRunnerOptions options = new TestRunnerOptions();
        CmdLineParser parser = new CmdLineParser(options);
        try {
            // parse the arguments.
            parser.parseArgument(args);
            GlobalConfig.updateFromOptions(options);
            System.out.println();
        } catch( CmdLineException e ) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
            System.err.println();
            return;
        }
        System.out.println(options);
    }

    private void runTests() {
        TestListenerAdapter tla = new TestListenerAdapter();
        TestNG tng = new TestNG();
        tng.addListener(tla);
        XmlSuite suite = new XmlSuite();
        suite.setName("TmpSuite");

        List<String> files = new ArrayList<>();
        files.add(FileTools.getCanonicalPathToResourceFile("/testng.xml"));
        suite.setSuiteFiles(files);

//        XmlTest test = new XmlTest(suite);
//        test.setName("TmpTest");
//        List<XmlClass> classes = new ArrayList<XmlClass>();
//        classes.add(new XmlClass("com.epam.tat.lesson7.ExampleTest"));
//        test.setXmlClasses(classes) ;

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        tng.setXmlSuites(suites);

        tng.setParallel(GlobalConfig.getParallelMode().getAlias());
        tng.setThreadCount(GlobalConfig.getThreadCount());
        tng.run();

    }

}
