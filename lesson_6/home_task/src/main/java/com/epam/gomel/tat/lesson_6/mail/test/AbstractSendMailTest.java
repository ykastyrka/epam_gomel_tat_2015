package com.epam.gomel.tat.lesson_6.mail.test;


import com.epam.gomel.tat.lesson_6.mail.bo.Letter;
import com.epam.gomel.tat.lesson_6.mail.bo.LetterBuilder;
import com.epam.gomel.tat.lesson_6.mail.service.MailGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AbstractSendMailTest extends AbstractBaseMailTest{
    protected MailGuiService mailGuiService = new MailGuiService();
    protected Letter letter = LetterBuilder.getDefaultLetterWithoutAttach();

    @BeforeClass(description = "Send mail", dependsOnMethods = "loginToAccount")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }
}
