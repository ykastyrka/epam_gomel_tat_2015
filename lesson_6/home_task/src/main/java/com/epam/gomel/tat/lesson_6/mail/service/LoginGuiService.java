package com.epam.gomel.tat.lesson_6.mail.service;

import com.epam.gomel.tat.lesson_6.common.bo.Account;
import com.epam.gomel.tat.lesson_6.common.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_6.mail.pages.MailLoginPage;
import com.epam.gomel.tat.lesson_6.mail.pages.MailboxBasePage;
import com.epam.gomel.tat.lesson_6.common.reporting.Logger;

public class LoginGuiService {
    private String errorMessageExpected = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    public void loginToAccountMailbox(Account account) {
        Logger.info("Login to account " + account.getEmail());
        MailboxBasePage mailbox = new MailLoginPage().open().successLogin(account.getLogin(), account.getPassword());
        String userEmail = mailbox.getUserEmail();
        if (userEmail == null || !userEmail.equals(account.getEmail())) {
            throw new TestCommonRuntimeException("Login failed. User Email : '" + userEmail + "'");
        }
    }

    public void checkUnsuccessfulLogin(Account account) {
        Logger.info("Check unsuccessful login to account " + account.getEmail());
        MailLoginPage loginPage = new MailLoginPage().open().unsuccessfulLogin(account.getLogin(), account.getPassword());
        String errorMessage = loginPage.getErrorMessageLogin();
        if (errorMessage == null || !errorMessage.equals(errorMessageExpected))
            throw new TestCommonRuntimeException("Login error message is not match expected output");
        Logger.info("Login error message is: " + errorMessage);
    }
}
