package com.epam.gomel.tat.lesson_6.mail.service;

import com.epam.gomel.tat.lesson_6.common.reporting.Logger;
import com.epam.gomel.tat.lesson_6.mail.bo.Letter;
import com.epam.gomel.tat.lesson_6.mail.pages.LetterContentPage;
import com.epam.gomel.tat.lesson_6.mail.pages.MailboxBasePage;

import java.io.IOException;

public class MailGuiService {

    public void sendMail(Letter letter) {
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxPage().openComposeMailPage().sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttach());
        Logger.info(getClass().getName() + ": message was send");
    }

    public void sendAttachMail(Letter letter) {
        sendMail(letter);
    }

    public void deleteMail(Letter letter) {
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxPage().deleteMail(letter.getSubject());
        Logger.info(getClass().getName() + ": message was deleted");
    }

    public void spamMail(Letter letter) {
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openInboxPage().spamMail(letter.getSubject());
        Logger.info(getClass().getName() + ": message was mark as spam");
    }

    public void noSpamMail(Letter letter) {
        MailboxBasePage mailbox = new MailboxBasePage();
        mailbox.openSpamPage().noSpamMail(letter.getSubject());
        Logger.info(getClass().getName() + ": message was mark as not spam");
    }

    public void checkMailInInbox(Letter letter) throws IOException {
        MailboxBasePage mailbox = new MailboxBasePage();
        LetterContentPage letterPage = mailbox.openInboxPage().openLetter(letter.getSubject());
        letterPage.checkLetterForEqual(letter);
        Logger.info("Message was successfully moved to inbox with subject: " + letter.getSubject());
    }

    public void checkMailInSent(Letter letter) throws IOException {
        MailboxBasePage mailbox = new MailboxBasePage();
        LetterContentPage letterPage = mailbox.openSentPage().openLetter(letter.getSubject());
        letterPage.checkLetterForEqual(letter);
        Logger.info("Message was sent successfully with subject: " + letter.getSubject());
    }

    public void checkMailInTrash(Letter letter) throws IOException {
        MailboxBasePage mailbox = new MailboxBasePage();
        LetterContentPage letterPage = mailbox.openTrashPage().openLetter(letter.getSubject());
        letterPage.checkLetterForEqual(letter);
        Logger.info("Message was deleted successfully with subject: " + letter.getSubject());
    }

    public void checkMailInSpam(Letter letter) throws IOException {
        MailboxBasePage mailbox = new MailboxBasePage();
        LetterContentPage letterPage = mailbox.openSpamPage().openLetter(letter.getSubject());
        letterPage.checkLetterForEqual(letter);
        Logger.info("Message was successfully moved to spam with subject: " + letter.getSubject());
    }
}
