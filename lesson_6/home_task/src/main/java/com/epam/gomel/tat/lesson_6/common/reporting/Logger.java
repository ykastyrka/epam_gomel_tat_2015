package com.epam.gomel.tat.lesson_6.common.reporting;

public class Logger {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Logger.class);

    public static void trace(String message) {
        log.trace(message);
    }

    public static void trace(String message, Throwable e) {
        log.trace(message, e);
    }

    public static void debug(String message) {
        log.debug(message);
    }

    public static void debug(String message, Throwable e) {
        log.debug(message, e);
    }

    public static void info(String message) {
        log.info(message);
    }

    public static void info(String message, Throwable e) {
        log.info(message, e);
    }

    public static void warn(String message) {
        log.warn(message);
    }

    public static void warn(String message, Throwable e) {
        log.warn(message, e);
    }

    public static void error(String message) {
        log.error(message);
    }

    public static void error(String message, Throwable e) {
        log.error(message, e);
    }

    public static void fatal(String message) {
        log.fatal(message);
    }

    public static void fatal(String message, Throwable e) {
        log.fatal(message, e);
    }
}
