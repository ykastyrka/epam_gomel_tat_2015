package com.epam.gomel.tat.lesson_6.common.runner;

import com.epam.gomel.tat.lesson_6.common.reporting.CustomTestNgListener;
import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> suites = new ArrayList<String>();
        testNG.addListener(new CustomTestNgListener());
        for(int i = 0; i < args.length; i++)
            suites.add(args[i]);
        testNG.setTestSuites(suites);
        testNG.run();
    }
}
