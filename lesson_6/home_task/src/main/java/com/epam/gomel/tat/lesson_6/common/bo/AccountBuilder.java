package com.epam.gomel.tat.lesson_6.common.bo;

public class AccountBuilder {
    public static final String INCORRECT_PASSWORD_TO_LOGIN = "12345";

    public static Account getDefaultAccount() {
        Account defaultAccount = DefaultAccounts.DEFAULT_MAIL.getAccount();
        return new Account(defaultAccount.getLogin(), defaultAccount.getPassword(), defaultAccount.getEmail());
    }

    public static Account getAccountWithIncorrectPassword() {
        Account defaultAccount = DefaultAccounts.DEFAULT_MAIL.getAccount();
        return new Account(defaultAccount.getLogin(), INCORRECT_PASSWORD_TO_LOGIN, defaultAccount.getEmail());
    }
}
