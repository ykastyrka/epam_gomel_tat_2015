package com.epam.gomel.tat.lesson_6.mail.bo;


import com.epam.gomel.tat.lesson_6.common.utils.FilesUtils;

import java.io.IOException;

public class LetterBuilder {
    public static Letter getDefaultLetterWithAttach() throws IOException {
        Letter letter = new DefaultLetters().getLetter();
        FilesUtils.createRandomTextFile(letter.getAttach());
        return letter;
    }

    public static Letter getDefaultLetterWithoutAttach() {
        Letter letter = new DefaultLetters().getLetter();
        letter.setAttach(null);
        return letter;
    }
}
