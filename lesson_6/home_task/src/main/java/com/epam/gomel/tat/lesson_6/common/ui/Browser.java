package com.epam.gomel.tat.lesson_6.common.ui;

import com.epam.gomel.tat.lesson_6.common.reporting.Logger;
import com.epam.gomel.tat.lesson_6.common.utils.FilesUtils;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.rmi.runtime.Log;

import java.util.concurrent.TimeUnit;

public class Browser {

    private static final String DOWNLOAD_DIR = "D:\\tmp\\download\\";
    private static final String FIREFOX_MIME_TYPES_TO_SAVE = "text/html, application/xhtml+xml, application/xml, application/csv, text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream, application/txt";
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 5;
    private static final int AJAX_TIMEOUT = 20;
    public static final int WAIT_ELEMENT_TIMEOUT = 20;

    private WebDriver driver;

    private static Browser instance = null;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser get() {
        if (instance != null) {
            return instance;
        }
        return instance = init();
    }

    private static Browser init() {
        WebDriver driver = new FirefoxDriver(getFireFoxProfile());
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        Logger.info("Browser initialized");
        return new Browser(driver);
    }

    private static FirefoxProfile getFireFoxProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", FIREFOX_MIME_TYPES_TO_SAVE);
        Logger.info("Browser profile configured");
        return profile;
    }

    public void open(String url) {
        driver.get(url);
        Logger.debug(getClass().getName() + ": url " + url + " opened");
    }

    public static void kill() {
        if(instance != null) {
            try {
                instance.driver.quit();
            } catch (Exception e) {
                Logger.error("Cannot kill browser", e);
            } finally {
                instance = null;
            }
            Logger.info("Browser killed");
        }
    }

    public String getDownloadDir() {
        return DOWNLOAD_DIR;
    }

    public String getText(By locator) {
        Logger.debug("Browser: getText By locator: " + locator);
        return driver.findElement(locator).getText();
    }

    public void click(By locator) {
        driver.findElement(locator).click();
        Logger.debug("Browser: clicked By locator: " + locator);
    }

    public void type(By locator, String text) {
        driver.findElement(locator).sendKeys(text);
        Logger.debug("Browser: typed By locator: " + locator + " with text: " + text);
    }

    public void attachFile(By locator, String text) {
        driver.findElement(locator).sendKeys(text);
        Logger.debug("Browser: attachFile By locator: " + locator + " with text: " + text);
    }

    public void submit(By locator) {
        driver.findElement(locator).submit();
        Logger.debug("Browser: submitted By locator: " + locator);
    }

    public void waitForDownloadFile(final String path) {
        Logger.debug("Browser: waitForDownloadFile " + path);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return FilesUtils.fileExists(path);
            }
        });
    }

    public boolean isPresent(By locator) {
        Logger.debug("Browser: isPresent By locator: " + locator);
        return driver.findElements(locator).size() > 0;
    }

    public void waitForPresent(final By locator) {
        Logger.debug("Browser: waitForPresent By locator: " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver driver) {
                return isPresent(locator);
            }
        });
    }

    public void waitForVisible(By locator) {
        Logger.debug("Browser: waitForVisible By locator: " + locator);
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForAjaxProcessed() {
        Logger.debug("Browser: waitForAjaxProcessed");
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

}
