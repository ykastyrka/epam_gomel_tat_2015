package com.epam.gomel.tat.lesson_6.mail.pages;

import org.openqa.selenium.By;

public class MailboxBasePage extends AbstractPage {
    public static final By HEADER_USER_EMAIL_LINK_LOCATOR = By.id("nb-1");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By TRASH_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");

    public MailInboxListPage openInboxPage() {
        browser.click(INBOX_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailInboxListPage();
    }

    public MailSentListPage openSentPage() {
        browser.click(SENT_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSentListPage();
    }

    public MailTrashListPage openTrashPage() {
        browser.click(TRASH_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailTrashListPage();
    }

    public MailSpamListPage openSpamPage() {
        browser.click(SPAM_LINK_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailSpamListPage();
    }

    public String getUserEmail() {
        return  browser.getText(HEADER_USER_EMAIL_LINK_LOCATOR);
    }
}
