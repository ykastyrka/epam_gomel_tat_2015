package com.epam.gomel.tat.lesson_6.mail.pages;

import org.openqa.selenium.By;

public class MailLoginPage extends AbstractPage {
    public static final String BASE_URL = "http://mail.yandex.ru";

    private static final By LOGIN_INPUT_LOCATOR = By.id("b-mail-domik-username11");
    private static final By PASSWORD_INPUT_LOCATOR = By.id("b-mail-domik-password11");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//input[@type='submit']");
    public static final By ERROR_MSG_LOGIN_LOCATOR = By.className("error-msg");

    public MailLoginPage open() {
        browser.open(BASE_URL);
        return this;
    }

    public MailboxBasePage successLogin(String login, String password) {
        executeLogin(login, password);
        return new MailboxBasePage();
    }

    public MailLoginPage unsuccessfulLogin(String login, String password) {
        executeLogin(login, password);
        return new MailLoginPage();
    }

    private void executeLogin(String login, String password) {
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.click(LOGIN_BUTTON_LOCATOR);
    }

    public String getErrorMessageLogin() {
        return browser.getText(ERROR_MSG_LOGIN_LOCATOR);
    }
}
