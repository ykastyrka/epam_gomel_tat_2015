package com.epam.gomel.tat.lesson_6.mail.pages;

import org.openqa.selenium.By;

public class MailSentListPage extends AbstractPage {
    public static final String MAIL_SENT_LINK_LOCATOR_TEMPLATE =
            "//label[text()='Отправленные']/ancestor::div[@class='block-messages']//span[text()='%s']/ancestor::a[1]";

    public LetterContentPage openLetter(String subject) {
        browser.click(By.xpath(String.format(MAIL_SENT_LINK_LOCATOR_TEMPLATE, subject)));
        browser.waitForAjaxProcessed();
        return new LetterContentPage();
    }
}
