package com.epam.gomel.tat.lesson_6.mail.pages;

import com.epam.gomel.tat.lesson_6.common.ui.Browser;

public abstract class AbstractPage {

    protected Browser browser;

    public AbstractPage() {
        this.browser = Browser.get();
    }

}
