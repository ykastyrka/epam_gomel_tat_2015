package com.epam.gomel.tat.lesson_6.mail.pages;


import com.epam.gomel.tat.lesson_6.common.exception.TestCommonRuntimeException;
import org.openqa.selenium.By;

public class MailSpamListPage extends AbstractPage{
    public static final String SPAM_CHECKBOX_INPUT_LOCATOR_TEMPLATE =
            "//label[text()='Спам']/ancestor::div[@class='block-messages']//span[text()='%s']/ancestor::div[1]//input";
    public static final By NO_SPAM_BUTTON_LOCATOR = By.xpath("//a[@data-action='notspam']");
    public static final String MAIL_SPAM_LINK_LOCATOR_TEMPLATE =
            "//label[text()='Спам']/ancestor::div[@class='block-messages']//span[text()='%s']/ancestor::a[1]";

    public void noSpamMail(String subject) {
        browser.click(By.xpath(String.format(SPAM_CHECKBOX_INPUT_LOCATOR_TEMPLATE, subject)));
        browser.click(NO_SPAM_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
//        if (browser.isPresent(By.xpath(String.format(SPAM_CHECKBOX_INPUT_LOCATOR_TEMPLATE, subject))))
//            throw new TestCommonRuntimeException("Message not marked as not spam");
    }

    public LetterContentPage openLetter(String subject) {
        browser.click(By.xpath(String.format(MAIL_SPAM_LINK_LOCATOR_TEMPLATE, subject)));
        browser.waitForAjaxProcessed();
        return new LetterContentPage();
    }
}
