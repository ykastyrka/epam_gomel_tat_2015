package com.epam.gomel.tat.lesson_6.common.bo;

public enum DefaultAccounts {
    DEFAULT_MAIL("ykastyrka", "yandex.ru", "ykastyrka@yandex.ru"),
    INCORRECT_MAIL("ykastyrka", "12345", "ykastyrka@yandex.ru");

    private Account account;

    DefaultAccounts(String login, String password, String email) {
        account = new Account(login, password, email);
    }

    public Account getAccount() {
        return account;
    }
}
