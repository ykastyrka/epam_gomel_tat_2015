package com.epam.gomel.tat.lesson_6.mail.test;


import com.epam.gomel.tat.lesson_6.mail.bo.Letter;
import com.epam.gomel.tat.lesson_6.mail.bo.LetterBuilder;
import com.epam.gomel.tat.lesson_6.mail.service.MailGuiService;
import org.testng.annotations.Test;

import java.io.IOException;


public class SendAttachMailTest extends AbstractBaseMailTest {
    private MailGuiService mailGuiService = new MailGuiService();
    private Letter letter;


    @Test(description = "Send attach mail")
    public void sendAttachMail() throws IOException {
        letter = LetterBuilder.getDefaultLetterWithAttach();
        mailGuiService.sendAttachMail(letter);
    }

    @Test(description = "Check attach mail in sent", dependsOnMethods = "sendAttachMail")
    public void checkMailInSent() throws IOException {
        mailGuiService.checkMailInSent(letter);
    }
}
