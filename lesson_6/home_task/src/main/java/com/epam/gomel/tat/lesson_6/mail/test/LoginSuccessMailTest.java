package com.epam.gomel.tat.lesson_6.mail.test;

import com.epam.gomel.tat.lesson_6.common.bo.Account;
import com.epam.gomel.tat.lesson_6.common.bo.AccountBuilder;
import com.epam.gomel.tat.lesson_6.mail.service.LoginGuiService;
import org.testng.annotations.Test;

public class LoginSuccessMailTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount= AccountBuilder.getDefaultAccount();

    @Test(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

}
