package com.epam.gomel.tat.lesson_6.mail.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MailInboxListPage extends AbstractPage {

    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final String CHECKBOX_INPUT_LOCATOR_TEMPLATE =
            "//div[@class='block-messages']//span[text()='%s']/ancestor::div[1]//input";
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");
    public static final By SPAM_BUTTON_LOCATOR = By.xpath("//a[@data-action='tospam']");
    public static final String MAIL_INBOX_LINK_LOCATOR_TEMPLATE =
            "//div[@class='block-messages']//span[text()='%s']/ancestor::a[1]";

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public void deleteMail(String subject) {
        browser.click(By.xpath(String.format(CHECKBOX_INPUT_LOCATOR_TEMPLATE, subject)));
        browser.click(DELETE_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
    }

    public void spamMail(String subject) {
        browser.click(By.xpath(String.format(CHECKBOX_INPUT_LOCATOR_TEMPLATE, subject)));
        browser.click(SPAM_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
    }

    public LetterContentPage openLetter(String subject) {
        browser.click(By.xpath(String.format(MAIL_INBOX_LINK_LOCATOR_TEMPLATE, subject)));
        browser.waitForAjaxProcessed();
        return new LetterContentPage();
    }
}
