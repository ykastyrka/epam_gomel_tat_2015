package com.epam.gomel.tat.lesson_6.mail.test;


import org.testng.annotations.Test;

import java.io.IOException;


public class SpamMailTest extends AbstractSendMailTest{
    @Test(description = "Spam mail")
    public void spamMail() {
        mailGuiService.spamMail(letter);
    }

    @Test(description = "Check mail in spam", dependsOnMethods = "spamMail")
    public void checkMailInSpam() throws IOException {
        mailGuiService.checkMailInSpam(letter);
    }
}
