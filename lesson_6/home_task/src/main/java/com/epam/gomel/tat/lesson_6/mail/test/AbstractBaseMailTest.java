package com.epam.gomel.tat.lesson_6.mail.test;


import com.epam.gomel.tat.lesson_6.common.bo.Account;
import com.epam.gomel.tat.lesson_6.common.bo.AccountBuilder;
import com.epam.gomel.tat.lesson_6.mail.bo.Letter;
import com.epam.gomel.tat.lesson_6.mail.bo.LetterBuilder;
import com.epam.gomel.tat.lesson_6.mail.service.LoginGuiService;
import com.epam.gomel.tat.lesson_6.mail.service.MailGuiService;
import org.testng.annotations.BeforeClass;


public abstract class AbstractBaseMailTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount = AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }
}
