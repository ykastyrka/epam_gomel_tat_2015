package com.epam.gomel.tat.lesson_6.common.utils;

import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;


public class FilesUtils {
    public static final int SIZE_OF_FILE_IN_CHARS = 1000;

    public static void createRandomTextFile(String path) throws IOException {
        FileUtils.writeStringToFile(new File(path), RandomUtils.getRandomString(SIZE_OF_FILE_IN_CHARS));
    }

    public static boolean equalTextFiles(String path1, String path2) throws IOException {
        File file1 = new File(path1);
        File file2 = new File(path2);
        boolean equalFiles = Files.equal(file1, file2);

        return equalFiles;
    }

    public static boolean fileExists(String path) {
        return (new File(path)).exists();
    }
}
