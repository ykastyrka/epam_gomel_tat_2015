package com.epam.gomel.tat.lesson_6.mail.test;


import com.epam.gomel.tat.lesson_6.common.bo.Account;
import com.epam.gomel.tat.lesson_6.common.bo.AccountBuilder;
import com.epam.gomel.tat.lesson_6.mail.service.LoginGuiService;
import org.testng.annotations.Test;

public class LoginUnsuccessfulMailTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account accountWithIncorrectPassword= AccountBuilder.getAccountWithIncorrectPassword();

    @Test(description = "Unsuccessful login to account mailbox")
    public void unsuccessfulLoginToAccount() {
        loginGuiService.checkUnsuccessfulLogin(accountWithIncorrectPassword);
    }
}
