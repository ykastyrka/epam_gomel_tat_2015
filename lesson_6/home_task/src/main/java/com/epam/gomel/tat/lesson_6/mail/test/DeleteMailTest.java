package com.epam.gomel.tat.lesson_6.mail.test;


import org.testng.annotations.Test;

import java.io.IOException;

public class DeleteMailTest extends AbstractSendMailTest{
    @Test(description = "Delete mail")
    public void deleteMail() {
        mailGuiService.deleteMail(letter);
    }

    @Test(description = "Check mail in trash", dependsOnMethods = "deleteMail")
    public void checkMailInTrash() throws IOException {
        mailGuiService.checkMailInTrash(letter);
    }
}
