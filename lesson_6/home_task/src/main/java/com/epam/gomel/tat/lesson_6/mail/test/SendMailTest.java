package com.epam.gomel.tat.lesson_6.mail.test;

import com.epam.gomel.tat.lesson_6.common.bo.Account;
import com.epam.gomel.tat.lesson_6.common.bo.AccountBuilder;
import com.epam.gomel.tat.lesson_6.mail.bo.Letter;
import com.epam.gomel.tat.lesson_6.mail.bo.LetterBuilder;
import com.epam.gomel.tat.lesson_6.mail.service.LoginGuiService;
import com.epam.gomel.tat.lesson_6.mail.service.MailGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

public class SendMailTest extends AbstractBaseMailTest {
    private MailGuiService mailGuiService = new MailGuiService();
    private Letter letter = LetterBuilder.getDefaultLetterWithoutAttach();

    @Test(description = "Send mail")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Check mail in sent", dependsOnMethods = "sendMail")
    public void checkMailInSent() throws IOException {
        mailGuiService.checkMailInSent(letter);
    }
}
