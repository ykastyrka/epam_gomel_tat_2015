package com.epam.gomel.tat.lesson_6.mail.pages;


import com.epam.gomel.tat.lesson_6.common.exception.TestCommonRuntimeException;
import com.epam.gomel.tat.lesson_6.common.reporting.Logger;
import com.epam.gomel.tat.lesson_6.common.utils.FilesUtils;
import com.epam.gomel.tat.lesson_6.mail.bo.Letter;
import org.openqa.selenium.By;

import java.io.IOException;

public class LetterContentPage extends AbstractPage {
    public static final By LETTER_RECEIVER_LOCATOR = By.xpath("//span[@class='b-message-head__email']");
    public static final By LETTER_SUBJECT_LOCATOR = By.cssSelector(".js-message-subject");
    public static final By LETTER_CONTENT_LOCATOR = By.xpath("//div[@class='block-message-body']//div[@class='b-message-body__content']/p");
    public static final By LETTER_ATTACH_LOCATOR = By.xpath("//div[@class='b-message-attachments_head']//a");
    public static final By LETTER_ATTACH_FILENAME_LOCATOR = By.xpath("//div[@class='b-message-attachments_head']//span[@class='b-file__text']");

    public void checkLetterForEqual(Letter letter) throws IOException {
        String receiver = browser.getText(LETTER_RECEIVER_LOCATOR);
        String receiverExpected = String.format(" <%s>", letter.getReceiver());
        if (receiver == null || !receiver.equals(receiverExpected))
            throw new TestCommonRuntimeException(getClass().getName() + ": Receiver letter: " + receiver + "is not match expected: " + receiverExpected);
        String subject = browser.getText(LETTER_SUBJECT_LOCATOR);
        if (subject == null || !subject.equals(letter.getSubject()))
            throw new TestCommonRuntimeException(getClass().getName() + ": Subject letter: " + subject + "is not match expected: " + letter.getSubject());
        String content = browser.getText(LETTER_CONTENT_LOCATOR);
        if (content == null || !content.equals(letter.getContent()))
            throw new TestCommonRuntimeException(getClass().getName() + ": Content letter: " + content + "is not match expected: " + letter.getContent());
        if (letter.getAttach() != null) {
            browser.click(LETTER_ATTACH_LOCATOR);
            String downloadedFileName = browser.getDownloadDir() + browser.getText(LETTER_ATTACH_FILENAME_LOCATOR);
            browser.waitForDownloadFile(downloadedFileName);
            if (!FilesUtils.equalTextFiles(letter.getAttach(), downloadedFileName))
                throw new TestCommonRuntimeException(getClass().getName() + ": Attach letter is not match expected");
        }
    }
}
