package com.epam.gomel.tat.lesson_6.mail.test;


import org.testng.annotations.Test;

import java.io.IOException;


public class NoSpamMailTest extends AbstractSendMailTest{
    @Test(description = "No spam mail")
    public void noSpamMail() {
        mailGuiService.spamMail(letter);
        mailGuiService.noSpamMail(letter);
    }

    @Test(description = "Check mail in inbox", dependsOnMethods = "noSpamMail")
    public void checkMailInInbox() throws IOException {
        mailGuiService.checkMailInInbox(letter);
    }
}
