package com.epam.gomel.tat.lesson_6.mail.bo;

import com.epam.gomel.tat.lesson_6.common.utils.RandomUtils;


public class DefaultLetters {
    public static final int SIZE_OF_RANDOM_STRING_FOR_SUBJECT = 10;
    public static final int SIZE_OF_RANDOM_STRING_FOR_CONTENT = 100;
    public static final int SIZE_OF_RANDOM_STRING_FOR_ATTACH_FILENAME = 10;

    public static final String DEFAULT_RECEIVER = "ykastyrka@yandex.ru";
    public static final String DEFAULT_SUBJECT = "Message subj ";
    public static final String DEFAULT_CONTENT = "Message content: ";
    public static final String DEFAULT_ATTACH = "D:\\tmp\\upload\\mailAttachment";

    private Letter letter;

    DefaultLetters() {
        letter = new Letter(getDefaultReceiver(), getDefaultSubject(), getDefaultContent(), getDefaultAttach());
    }

    private String getDefaultReceiver() {
        return DEFAULT_RECEIVER;
    }

    private String getDefaultSubject() {
        return DEFAULT_SUBJECT + RandomUtils.getRandomString(SIZE_OF_RANDOM_STRING_FOR_SUBJECT);
    }

    private String getDefaultContent() {
        return DEFAULT_CONTENT + RandomUtils.getRandomString(SIZE_OF_RANDOM_STRING_FOR_CONTENT);
    }

    private String getDefaultAttach() {
        return DEFAULT_ATTACH + RandomUtils.getRandomString(SIZE_OF_RANDOM_STRING_FOR_ATTACH_FILENAME);
    }

    public Letter getLetter() {
        return letter;
    }
}
