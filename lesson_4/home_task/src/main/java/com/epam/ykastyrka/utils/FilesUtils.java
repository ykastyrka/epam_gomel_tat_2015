package com.epam.ykastyrka.utils;

import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Yury_Kastyrka on 3/16/2015.
 */

public class FilesUtils {
    public static void createRandomTextFile(String path) throws IOException {
        String randomString = UUID.randomUUID().toString();
        FileUtils.writeStringToFile(new File(path), randomString);
    }

    public static boolean equalTextFiles(String path1, String path2) throws IOException {
        File file1 = new File(path1);
        File file2 = new File(path2);
        boolean equalFiles = Files.equal(file1, file2);

        return equalFiles;
    }
}
