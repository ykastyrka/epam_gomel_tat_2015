package com.epam.ykastyrka.test;

import com.epam.ykastyrka.utils.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by Yury_Kastyrka on 2/27/2015.
 */

public class MailLoginSuccessTest {
    // AUT data
    public static final String SITE_NAME_LINK = "http://www.yandex.ru";

    // WebDriver setup
    private WebDriver driver;
    public static final int DRIVER_IMPL_WAIT_TIMEOUT_SECONDS = 5;

    // Locators
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWD_INPUT_LOCATOR = By.name("passwd");
    public static final By HEADER_USER_NAME_LINK_LOCATOR = By.id("nb-1");
    public static final By LOG_OUT_LINK_LOCATOR = By.xpath(
            "//div[@id='user-dropdown-popup']//div[@class='b-mail-dropdown__item'][last()]/a");

    // Test data
    private String userLogin = "ykastyrka";
    private String userPasswd = "yandex.ru";
    private String userName = "ykastyrka@yandex.ru";

    @BeforeClass(description = "Init browser")
    public void initBrowser() {
        Logger.log.info("Init browser");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(DRIVER_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.get(SITE_NAME_LINK);
    }

    @Test(description = "Mail login success test")
    public void mailLoginSuccessTest() {
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passwdInput = driver.findElement(PASSWD_INPUT_LOCATOR);
        passwdInput.sendKeys(userPasswd);
        passwdInput.submit();

        WebElement headerUserNameLink = driver.findElement(HEADER_USER_NAME_LINK_LOCATOR);
        String userNameText = headerUserNameLink.getText();
        Logger.log.info("User Name is " + userNameText);

        Assert.assertEquals(userNameText, userName, String.format("User Name is not %s", userName));

        headerUserNameLink.click();
        WebElement logOutLink = driver.findElement(LOG_OUT_LINK_LOCATOR);
        logOutLink.click();
    }

    @AfterClass(description = "Close browser")
    public void closeBrowser() {
        driver.close();
        Logger.log.info("Close browser");
    }
}
