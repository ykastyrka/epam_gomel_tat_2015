package com.epam.ykastyrka.test;

import com.epam.ykastyrka.utils.Logger;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Yury_Kastyrka on 3/17/2015.
 */

public class MailNoSpamTest {
    // AUT data
    public static final String SITE_NAME_LINK = "http://www.yandex.ru";

    // WebDriver setup
    private WebDriver driver;
    public static final int DRIVER_IMPL_WAIT_TIMEOUT_SECONDS = 10;
    public static final int TIMEOUT_MAIL_SPAM_SECONDS = 25;

    // Locators
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By MAIL_TO_INPUT_LOCATOR = By.xpath(
            "//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By MAIL_SUBJ_INPUT_LOCATOR = By.name("subj");
    public static final By TEXT_AREA_INPUT_LOCATOR = By.name("send");
    public static final By MAIL_SEND_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//div[@class='block-folders']//a[@href='#inbox']");
    public static final String CHECKBOX_INPUT_LOCATOR_TEMPLATE =
            "//div[@class='block-messages']//span[text()='%s']/ancestor::div[1]//input";
    public static final By SPAM_BUTTON_LOCATOR = By.xpath("//a[@data-action='tospam']");
    public static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");
    public static final String SPAM_CHECKBOX_INPUT_LOCATOR_TEMPLATE =
            "//label[text()='Спам']/ancestor::div[@class='block-messages']//span[text()='%s']/ancestor::div[1]//input";
    public static final By NO_SPAM_BUTTON_LOCATOR = By.xpath("//a[@data-action='notspam']");
    public static final String MAIL_SPAM_LINK_LOCATOR_TEMPLATE =
            "//label[text()='Спам']/ancestor::div[@class='block-messages']//span[text()='%s']/ancestor::a[1]";
    public static final String MAIL_INBOX_LINK_LOCATOR_TEMPLATE =
            "//div[@class='block-messages']//span[text()='%s']/ancestor::a[1]";


    // Test data
    private String userLogin = "ykastyrka";
    private String userPasswd = "yandex.ru";
    private String mailToData = "ykastyrka@yandex.ru";
    private String mailSubjData = "Message subj " + Math.random() * 1000000;
    private String textAreaData = "Message content: " + Math.random() * 1000000;

    @BeforeClass(description = "Init browser")
    public void initBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(DRIVER_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.get(SITE_NAME_LINK);
    }

    public void mailLogin() {
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passwdInput = driver.findElement(PASSWD_INPUT_LOCATOR);
        passwdInput.sendKeys(userPasswd);
        passwdInput.submit();
    }

    public void mailSendTest() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();

        WebElement mailToInput = driver.findElement(MAIL_TO_INPUT_LOCATOR);
        mailToInput.sendKeys(mailToData);
        WebElement mailSubjInput = driver.findElement(MAIL_SUBJ_INPUT_LOCATOR);
        mailSubjInput.sendKeys(mailSubjData);
        WebElement textAreaInput = driver.findElement(TEXT_AREA_INPUT_LOCATOR);
        textAreaInput.sendKeys(textAreaData);
        WebElement mailSendButton = driver.findElement(MAIL_SEND_BUTTON_LOCATOR);
        mailSendButton.click();
    }

    @BeforeClass(description = "Mail login and send message", dependsOnMethods = "initBrowser")
    public void mailLoginAndSendMessage() {
        mailLogin();
        mailSendTest();
    }

    @Test(description = "Mail no spam test")
    public void mailNoSpamTest() {
        new WebDriverWait(driver, TIMEOUT_MAIL_SPAM_SECONDS).until(
                ExpectedConditions.presenceOfElementLocated(INBOX_LINK_LOCATOR));
        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        new WebDriverWait(driver, TIMEOUT_MAIL_SPAM_SECONDS).until(ExpectedConditions.presenceOfElementLocated(
                By.xpath(String.format(CHECKBOX_INPUT_LOCATOR_TEMPLATE, mailSubjData))));
        WebElement checkboxInput = driver.findElement(By.xpath(String.format(
                CHECKBOX_INPUT_LOCATOR_TEMPLATE, mailSubjData)));
        checkboxInput.click();
        WebElement spamButton = driver.findElement(SPAM_BUTTON_LOCATOR);
        spamButton.click();

        new WebDriverWait(driver, TIMEOUT_MAIL_SPAM_SECONDS).until(
                ExpectedConditions.presenceOfElementLocated(SPAM_LINK_LOCATOR));
        WebElement spamLink = driver.findElement(SPAM_LINK_LOCATOR);
        spamLink.click();

        new WebDriverWait(driver, TIMEOUT_MAIL_SPAM_SECONDS).until(ExpectedConditions.presenceOfElementLocated(
                By.xpath(String.format(SPAM_CHECKBOX_INPUT_LOCATOR_TEMPLATE, mailSubjData))));
        List<WebElement> spamCheckboxInputList = driver.findElements(By.xpath(String.format(
                SPAM_CHECKBOX_INPUT_LOCATOR_TEMPLATE, mailSubjData)));
        for (WebElement spamCheckboxInput : spamCheckboxInputList)
            spamCheckboxInput.click();
        new WebDriverWait(driver, TIMEOUT_MAIL_SPAM_SECONDS).until(
                ExpectedConditions.presenceOfElementLocated(NO_SPAM_BUTTON_LOCATOR));
        WebElement noSpamButton = driver.findElement(NO_SPAM_BUTTON_LOCATOR);
        noSpamButton.click();
    }

    public void waitForNotPresenceElement(final By locator) {
        new WebDriverWait(driver, TIMEOUT_MAIL_SPAM_SECONDS).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return webDriver.findElements(locator).size() == 0;
            }
        });
    }

    @Test(description = "Mail check no spam message", dependsOnMethods = "mailNoSpamTest")
    public void mailCheckNoSpam() {
        waitForNotPresenceElement(By.xpath(String.format(
                MAIL_SPAM_LINK_LOCATOR_TEMPLATE, mailSubjData)));
        driver.navigate().refresh();
        new WebDriverWait(driver, TIMEOUT_MAIL_SPAM_SECONDS).until(
                ExpectedConditions.presenceOfElementLocated(INBOX_LINK_LOCATOR));
        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        new WebDriverWait(driver, TIMEOUT_MAIL_SPAM_SECONDS).until(ExpectedConditions.presenceOfElementLocated(
                By.xpath(String.format(MAIL_INBOX_LINK_LOCATOR_TEMPLATE, mailSubjData))));

        Logger.log.info("Message was successfully moved back to inbox from spam with subject: " + mailSubjData);
    }

    @AfterClass(description = "Close browser")
    public void closeBrowser() {
        driver.close();
    }
}
