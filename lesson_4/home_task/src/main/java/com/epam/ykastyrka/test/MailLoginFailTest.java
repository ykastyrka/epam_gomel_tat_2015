package com.epam.ykastyrka.test;

import com.epam.ykastyrka.utils.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Yury_Kastyrka on 3/5/2015.
 */

public class MailLoginFailTest {
    // AUT data
    public static final String SITE_NAME_LINK = "http://www.yandex.ru";

    // WebDriver setup
    private WebDriver driver;
    public static final int DRIVER_IMPL_WAIT_TIMEOUT_SECONDS = 5;

    // Locators
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWD_INPUT_LOCATOR = By.name("passwd");
    public static final By ERROR_MSG_ELEMENT_LOCATOR = By.className("error-msg");

    // Test data
    private String userLogin = "ykastyrka";
    private String userPasswdIncorrect = "12345";
    private String errorMsgExpected = "Неправильная пара логин-пароль! Авторизоваться не удалось.";
    private String errorMsgAssert = "Error msg is not match expected output";

    @BeforeClass(description = "Init browser")
    public void initBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(DRIVER_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.get(SITE_NAME_LINK);
    }

    @Test(description = "Mail login fail test")
    public void mailLoginFailTest() {
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passwdInput = driver.findElement(PASSWD_INPUT_LOCATOR);
        passwdInput.sendKeys(userPasswdIncorrect);
        passwdInput.submit();

        WebElement errorMsgElement = driver.findElement(ERROR_MSG_ELEMENT_LOCATOR);
        String errorMsg = errorMsgElement.getText();
        Logger.log.info("errorMsg is: " + errorMsg);
        Assert.assertEquals(errorMsg, errorMsgExpected, errorMsgAssert);
    }

    @AfterClass(description = "Close browser")
    public void closeBrowser() {
        driver.close();
    }
}
