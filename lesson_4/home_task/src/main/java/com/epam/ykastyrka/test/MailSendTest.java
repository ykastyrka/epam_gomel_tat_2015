package com.epam.ykastyrka.test;

import com.epam.ykastyrka.utils.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Yury_Kastyrka on 3/10/2015.
 */

public class MailSendTest {
    // AUT data
    public static final String SITE_NAME_LINK = "http://www.yandex.ru";

    // WebDriver setup
    private WebDriver driver;
    public static final int DRIVER_IMPL_WAIT_TIMEOUT_SECONDS = 10;
    public static final int TIMEOUT_MAIL_SENT_SECONDS = 10;

    // Locators
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By MAIL_TO_INPUT_LOCATOR = By.xpath(
            "//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By MAIL_SUBJ_INPUT_LOCATOR = By.name("subj");
    public static final By TEXT_AREA_INPUT_LOCATOR = By.name("send");
    public static final By MAIL_SEND_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By MAIL_SEND_MSG_CONFIRM_LOCATOR = By.className("b-done-title");
    public static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final String MAIL_SENT_LINK_LOCATOR_TEMPLATE =
            "//label[text()='Отправленные']/ancestor::div[@class='block-messages']//span[text()='%s']/ancestor::a[1]";

    // Test data
    private String userLogin = "ykastyrka";
    private String userPasswd = "yandex.ru";
    private String mailToData = "ykastyrka@yandex.ru";
    private String mailSubjData = "Message subj " + Math.random() * 1000000;
    private String textAreaData = "Message content: " + Math.random() * 1000000;
    private String mailSendMsgConfirmExpected = "Письмо успешно отправлено.";
    private String mailSendMsgConfirmAssert = "Mail send msg confirm is not match expected output";

    @BeforeClass(description = "Init browser")
    public void initBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(DRIVER_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.get(SITE_NAME_LINK);
    }

    @Test(description = "Mail login")
    public void mailLogin() {
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passwdInput = driver.findElement(PASSWD_INPUT_LOCATOR);
        passwdInput.sendKeys(userPasswd);
        passwdInput.submit();
    }

    @Test(description = "Mail send test", dependsOnMethods = "mailLogin")
    public void mailSendTest() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();

        WebElement mailToInput = driver.findElement(MAIL_TO_INPUT_LOCATOR);
        mailToInput.sendKeys(mailToData);
        WebElement mailSubjInput = driver.findElement(MAIL_SUBJ_INPUT_LOCATOR);
        mailSubjInput.sendKeys(mailSubjData);
        WebElement textAreaInput = driver.findElement(TEXT_AREA_INPUT_LOCATOR);
        textAreaInput.sendKeys(textAreaData);
        WebElement mailSendButton = driver.findElement(MAIL_SEND_BUTTON_LOCATOR);
        mailSendButton.click();
    }

    @Test(description = "Mail check send", dependsOnMethods = "mailSendTest")
    public void mailCheckSend() {
        String mailSendMsgConfirm = driver.findElement(MAIL_SEND_MSG_CONFIRM_LOCATOR).getText();
        Logger.log.info("mailSendMsgConfirm is: " + mailSendMsgConfirm);
        Assert.assertEquals(mailSendMsgConfirm, mailSendMsgConfirmExpected, mailSendMsgConfirmAssert);

        new WebDriverWait(driver, TIMEOUT_MAIL_SENT_SECONDS).until(ExpectedConditions.presenceOfElementLocated(
                SENT_LINK_LOCATOR));
        WebElement sentLink = driver.findElement(SENT_LINK_LOCATOR);
        sentLink.click();

        new WebDriverWait(driver, TIMEOUT_MAIL_SENT_SECONDS).until(ExpectedConditions.presenceOfElementLocated(
                By.xpath(String.format(MAIL_SENT_LINK_LOCATOR_TEMPLATE, mailSubjData))));
        Logger.log.info("Message was sent successfully with subject: " + mailSubjData);
    }

    @AfterClass(description = "Close browser")
    public void closeBrowser() {
        driver.close();
    }
}
