package com.epam.gomel.tat.lesson_5.pages;

import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 3/17/2015.
 */
public class MailInboxListPage extends AbstractBasePage {

    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }
}
